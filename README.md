# `rtic-hid`

> Project to play around with RTIC and USB HID devices

[![pipeline status](https://gitlab.com/r.kyle.murphy/rtic-hid/badges/master/pipeline.svg)](https://gitlab.com/r.kyle.murphy/rtic-hid/-/commits/master)

Currently, all it does is wiggle your mouse back and forth every second.
