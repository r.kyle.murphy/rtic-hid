
#![deny(unsafe_code)]
#![deny(warnings)]
#![no_main]
#![no_std]

use panic_halt as _;
use stm32f4xx_hal as _; // memory layout

/// Terminates the application and makes `probe-run` exit with exit-code = 0
pub fn exit() -> ! {
    loop {
        cortex_m::asm::bkpt();
    }
}

#[rtic::app(device = stm32f4xx_hal::stm32, peripherals = true, dispatchers = [EXTI1, EXTI2])]
mod app {
    use stm32f4xx_hal as _;
    use stm32f4xx_hal::prelude::*;
    use usb_device::prelude::*;
    use stm32f4xx_hal::otg_fs::USB;
    use stm32f4xx_hal::otg_fs::UsbBusType;
    use stm32f4xx_hal::gpio::GpioExt;
    use usbd_hid::descriptor::generator_prelude::*;
    use usbd_hid::descriptor::MouseReport;
    use usbd_hid::hid_class::HIDClass;
    use usb_device::bus::UsbBusAllocator;
    use dwt_systick_monotonic::DwtSystick;
    use rtic::time::duration::Seconds;

    #[resources]
    struct Resources {
        #[init(false)]
        direction: bool,
        usb_hid: HIDClass<'static, UsbBusType>,
        usb_dev: UsbDevice<'static, UsbBusType>
    }

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = DwtSystick<48_000_000>; // 48 MHz

    const TWITCH_PERIOD: u32 = 5_u32;

    #[init]
    fn init(mut cx: init::Context) -> (init::LateResources, init::Monotonics) {
        static mut EP_MEMORY: [u32; 1024] = [0; 1024];
        static mut USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None;

        let rcc = cx.device.RCC.constrain();
        let clocks = rcc.cfgr
            .use_hse(25.mhz())
            .sysclk(48.mhz())
            .require_pll48clk()
            .freeze();

        let mono = DwtSystick::new(
            &mut cx.core.DCB,
            cx.core.DWT,
            cx.core.SYST,
            clocks.hclk().0,
        );

        let gpioa = cx.device.GPIOA.split();

        let usb = USB {
            usb_global: cx.device.OTG_FS_GLOBAL,
            usb_device: cx.device.OTG_FS_DEVICE,
            usb_pwrclk: cx.device.OTG_FS_PWRCLK,
            pin_dm: gpioa.pa11.into_alternate_af10(),
            pin_dp: gpioa.pa12.into_alternate_af10(),
            hclk: clocks.hclk(),
        };

        USB_BUS.replace(UsbBusType::new(usb, EP_MEMORY));
        let usb_hid = HIDClass::new(USB_BUS.as_ref().unwrap(), MouseReport::desc(), 60);

        let usb_dev = UsbDeviceBuilder::new(USB_BUS.as_ref().unwrap(), UsbVidPid(0xc410, 0x0000))
            .manufacturer("Wiggle Tech")
            .product("Mouse")
            .serial_number("CAT")
            .device_class(0) // class misc
            .build();

        twitch::spawn_after(Seconds(TWITCH_PERIOD)).ok();

        return (
            init::LateResources { usb_hid, usb_dev },
            init::Monotonics(mono),
        );
    }

    #[link_section = ".data.twitch"]
    #[task(priority=2, resources=[usb_hid, direction])]
    fn twitch(cx: twitch::Context) {
        (cx.resources.direction, cx.resources.usb_hid).lock(|direction, hid| {
            let input = MouseReport {
                x: if *direction { 4 } else { -4 } ,
                y: 0,
                buttons: 0,
                wheel: 0
            };
            *direction = !*direction;
            let _ = hid.push_input(&input).ok().unwrap_or(0);
        });
        twitch::spawn_after(Seconds(TWITCH_PERIOD)).ok();
    }

    #[link_section = ".data.usb"]
    #[task(binds=OTG_FS, priority=1, resources=[usb_hid, usb_dev])]
    fn usb_handler(cx: usb_handler::Context) {
        (cx.resources.usb_dev, cx.resources.usb_hid).lock(|dev, hid| {
            if !dev.poll(&mut [hid]) {
                return;
            }
        });
    }
}
